<?php require "./phpOperation.php"; ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PHP Collection/Loop Control Structures and Array Manipulation</title>
</head>
<body>
<h1>Repetition Control structures</h1>

<h2>While loop</h2>

<span>
    <?php whileLoop(); ?>
</span>

<h3>Numbers divisible by 5</h3>
<span>
    <?php printDivisibleBy5(); ?>
</span>

<h2>Do-While loop</h2>

<span>
    <?php doWhileLoop(); ?>	
</span>

<h2>Mini Activity 2</h2>

<span>
    <?php getOddNumbers(); ?>	
</span>

<h2>For Loop</h2>

<span>
    <?php forLoop(); ?>
</span>

<?php echo $firstName; ?>
<h2>Array Manipulation</h2>

<h3>Associative Array</h3>

<p>
    <?php echo $gradePeriods['fourthGrade']; ?>
</p>

<p>
    <?php var_dump($tasks); ?>
</p>

<p>
    <?php var_dump($gradePeriods); ?>
</p>

<p>
    <?php var_dump($students); ?>
</p>

<p>
    <?php var_dump($students[0]); ?>
</p>
<p>
    <?php 
        // var_dump($students[0]['studentName']);
        print_r($students[0]);
    ?>
</p>

<!-- mas pretty way of using print_r -->
<!-- pre html tag defines a preformatted text -->
<pre> 
    <?php print_r($students[0]['studentName']); ?>	
</pre>

<ul>
    <?php printTasks($tasks); ?>
</ul>

<ul>
    <?php printComputers($computerBrands); ?>
</ul>

<?php 
    foreachLoop($tasks);
 ?>

 <ul>
     <?php printGrades($gradePeriods); ?>
 </ul>

 <ul>
     <?php printHeroes($heroes); ?>
 </ul>

 <pre>
     <?php print_r($tasks); ?>
 </pre>


  


</body>
</html>